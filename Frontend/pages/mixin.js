import GuestPage from '../components/GuestPage.vue'
import { setToken } from 'tools/backend'

export default {
  components: { GuestPage },
  data () {
    return {
      user: {
        email: '',
        password: ''
      }
    }
  },
  methods: {
    loggedIn (token) {
      setToken(token)

      this.$router.replace(this.$config.defaultRoute)
    }
  }
}
