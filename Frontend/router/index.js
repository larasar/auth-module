import AuthLayout from '../layouts/Auth'
import RegisterPage from '../pages/Register'
import LoginPage from '../pages/Login'
import ForgotPasswordPage from '../pages/ForgotPassword'
import ResetPasswordPage from '../pages/ResetPassword'
import VerifyEmailPage from '../pages/VerifyEmail'

export default {
  component: AuthLayout,
  children: [
    {
      name: 'auth.register',
      path: 'register',
      component: RegisterPage
    },
    {
      name: 'auth.login',
      path: 'login',
      component: LoginPage
    },
    {
      name: 'auth.forgot-password',
      path: 'forgot-password',
      component: ForgotPasswordPage
    },
    {
      name: 'auth.reset-password',
      path: 'reset-password',
      component: ResetPasswordPage
    },
    {
      name: 'auth.verify-email',
      path: 'verify-email',
      component: VerifyEmailPage
    }
  ]
}
