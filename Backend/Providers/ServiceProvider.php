<?php

namespace Module\Auth\Backend\Providers;

use Larasar\Helpers\Frontend;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use Module\Auth\Backend\Models\User;

class ServiceProvider extends BaseServiceProvider
{
  /**
   * Register services.
   *
   * @return void
   */
  public function register()
  {
    if (!app()->configurationIsCached()) {
      $this->mergeConfigFrom(__DIR__ . '/../config/auth-module.php', 'auth-module');
    }
  }

  /**
   * Bootstrap services.
   *
   * @return void
   */
  public function boot()
  {
    if (app()->runningInConsole()) {
      $this->publishes([
        __DIR__ . '/../config/auth-module.php' => config_path('auth-module.php'),
      ], 'auth-module-config');
    }

    ResetPassword::createUrlUsing(fn (User $user, string $token) => Frontend::view('auth.reset-password', ['token' => $token, 'email' => $user->email]));

    if (is_a(User::class, MustVerifyEmail::class)) {
      VerifyEmail::createUrlUsing(fn (User $user) => Frontend::view('auth.verify-email', ['code' => sha1($user->getEmailForVerification())]));
    }
  }
}
