<?php

namespace Module\Auth\Backend\Providers;

use Laravel\Sanctum\SanctumServiceProvider;

class SanctumProvider extends SanctumServiceProvider
{
  public function register()
  {
    config([
      'auth.guards.sanctum' => array_merge([
        'driver' => 'sanctum',
        'provider' => null,
      ], config('auth.guards.sanctum', [])),
    ]);

    if (!app()->configurationIsCached()) {
      $this->mergeConfigFrom(__DIR__ . '/../config/sanctum.php', 'sanctum');
    }
  }

  public function boot()
  {
    if (app()->runningInConsole()) {
      $this->publishes([
        __DIR__ . '/../config/sanctum.php' => config_path('sanctum.php'),
      ], 'larasar-sanctum-config');
    }

    $this->defineRoutes();
    $this->configureGuard();
    $this->configureMiddleware();
  }
}
