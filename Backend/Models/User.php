<?php

namespace Module\Auth\Backend\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as AuthUser;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Str;
use Laraquick\Models\Traits\Helper;

class User extends AuthUser
{
  use HasApiTokens, HasFactory, Notifiable, SoftDeletes, Helper;

  /**
   * The attributes that are mass assignable.
   *
   * @var string[]
   */
  protected $fillable = [
    'name',
    'email',
    'password',
  ];

  /**
   * The attributes that should be hidden for serialization.
   *
   * @var array
   */
  protected $hidden = [
    'password',
    'remember_token',
  ];

  /**
   * The attributes that should be cast.
   *
   * @var array
   */
  protected $casts = [
    'email_verified_at' => 'datetime',
  ];

  protected $appends = ['first_name', 'middle_name', 'last_name'];

  public function getFirstNameAttribute(): string
  {
    return Str::before($this->name ?? ' ', ' ');
  }

  public function getLastNameAttribute(): string
  {
    return Str::afterLast($this->name ?? ' ', ' ');
  }

  public function setName(string $firstName, string $lastName): self
  {
    $this->name = $firstName . ' ' . $lastName;

    return $this;
  }
}
