<?php

use Illuminate\Support\Facades\Route;
use Module\Auth\Backend\Http\Controllers\ForgotPasswordController;
use Module\Auth\Backend\Http\Controllers\LoginController;
use Module\Auth\Backend\Http\Controllers\RecaptchaController;
use Module\Auth\Backend\Http\Controllers\RegisterController;
use Module\Auth\Backend\Http\Controllers\ResetPasswordController;
use Module\Auth\Backend\Http\Controllers\VerificationController;

Route::middleware('throttle:60,1')->group(function () {
  Route::post('register', [RegisterController::class, 'postRegister'])->name('register');
  Route::post('login', [LoginController::class, 'postLogin'])->name('login');
  Route::post('password/email', [ForgotPasswordController::class, 'postSendResetLinkEmail'])->name('password.email');
  Route::post('password/reset', [ResetPasswordController::class, 'postReset'])->name('password.update');
  Route::post('email/verify', [VerificationController::class, 'verify'])->name('verification.verify');
  Route::post('recaptcha/verify', [RecaptchaController::class, 'verify'])->name('recaptcha.verify');
});
