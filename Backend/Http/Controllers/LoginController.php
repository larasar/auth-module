<?php

namespace Module\Auth\Backend\Http\Controllers;

use Larasar\Helpers\Respond;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Module\Auth\Backend\Http\Controllers\Traits\Authable;
use Module\Auth\Backend\Models\User;

class LoginController extends Controller
{
  /*
  |--------------------------------------------------------------------------
  | Login Controller
  |--------------------------------------------------------------------------
  |
  | This controller handles authenticating users for the application and
  | redirecting them to your home screen. The controller uses a trait
  | to conveniently provide its functionality to your applications.
  |
  */

  use AuthenticatesUsers, Authable;

  protected function authenticated(Request $request, $user)
  {
    return $this->tokenResponse($user);
  }

  protected function sendFailedLoginResponse(Request $request)
  {
    $user = User::withTrashed()->where('email', $request->email)->first();

    if (!empty($user->deleted_at)) {
      return Respond::error('Account deactivated', null, Response::HTTP_NOT_ACCEPTABLE);
    }

    return Respond::error('Invalid email/password', null, Response::HTTP_UNPROCESSABLE_ENTITY);
  }

  /**
   * Login
   *
   * @group Auth
   *
   * @bodyParam email string required The user's email address. Example: john.doe@gmail.com
   * @bodyParam password string required The user's password
   *
   * @responseFile app/test-responses/auth/login/success
   * @responseFile 400 app/test-responses/auth/login/wrong-email-address
   *
   * @param Request $request
   * @return JsonResponse
   */
  public function postLogin(Request $request)
  {
    if (!empty($request->restore) && !empty($request->email)) {
      User::withTrashed()->where('email', $request->email)->restore();
    }

    return $this->login($request);
  }

  /**
   * Logout
   *
   * @group Auth
   *
   * @responseFile app/test-responses/auth/logout
   *
   * @param Request $request
   * @return JsonResponse
   */
  public function postLogout(Request $request)
  {
    $request->user()->currentAccessToken()->delete();

    $request->session()->invalidate();

    $request->session()->regenerateToken();

    return Respond::success();
  }

  public function createToken(Request $request)
  {
    return $this->login($request);
  }
}
