<?php

namespace Module\Auth\Backend\Http\Controllers;

use Larasar\Helpers\Respond;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Module\Auth\Backend\Http\Controllers\Traits\Authable;
use Module\Auth\Backend\Models\User;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords, Authable;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    protected function sendResetResponse(Request $request, $response)
    {
        $user = User::where('email', $request->email)->first();

        return Respond::success(['token' => $this->getToken($user)]);
    }

    protected function sendResetFailedResponse(Request $request, $response)
    {
        return Respond::error(trans($response));
    }

    /**
     * Reset password (2)
     *
     * Sets a new password
     *
     * @group Auth
     *
     * @bodyParam token string required The token sent to the user's email address
     * @bodyParam email string required The email of the user. Example: john.doe@gmail.com
     * @bodyParam password string required The new password. Example: SomeVeryLongPassword1234
     * @bodyParam password_confirmation string required The same value as the password field. Example: SomeVeryLongPassword1234
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function postReset(Request $request)
    {
        return $this->reset($request);
    }

}
