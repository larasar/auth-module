<?php

namespace Module\Auth\Backend\Http\Controllers;

use Larasar\Helpers\Respond;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    protected function sendResetLinkResponse(Request $request, $response)
    {
        return Respond::success('Password reset email sent');
    }

    protected function sendResetLinkFailedResponse(Request $request, $response)
    {
        return $this->sendResetLinkResponse($request, $response);
    }

    /**
     * Reset Password (1)
     *
     * Send password reset email
     *
     * @group Auth
     *
     * @bodyParam email string required The email to send the link to. Example: john.doe@gmail.com
     *
     * @responseFile app/test-responses/auth/reset-password-1/success
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function postSendResetLinkEmail(Request $request)
    {
        return $this->sendResetLinkEmail($request);
    }

}
