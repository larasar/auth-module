<?php

namespace Module\Auth\Backend\Http\Controllers;

use Larasar\Helpers\Respond;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Module\Auth\Backend\Http\Controllers\Traits\Authable;
use Module\Auth\Backend\Models\User;

/**
 * @group Auth
 *
 * Verify email
 */
class VerificationController extends Controller
{

    use Authable;

    public function verify(Request $request)
    {
        $request->validate([
            'code' => 'required|string'
        ]);

        $user = User::where('verification_code', $request->code)->first();

        if (!$user) {
            return Respond::error('Account is already verified');
        }

        if ($user->new_email) {
            $user->email = $user->new_email;
        }

        $user->verification_code = null;
        $user->new_email = null;
        $user->email_verified_at = Carbon::now();
        $user->save();

        return Respond::success('Account verified successfully');
    }

    public function resend()
    {
        $user = Auth::user();

        if ($user->is_verified) {
            return Respond::error('Account is already verified');
        }

        $user->verification_code = Str::random(20);
        $user->save();

        if ($user->new_email) {
            $user->sendNewEmailVerificationNotification();
        } else {
            $user->sendEmailVerificationNotification();
        }

        return Respond::success('Verification email sent');
    }

}
