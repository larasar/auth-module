<?php

namespace Module\Auth\Backend\Http\Controllers;

use Larasar\Helpers\Respond;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laraquick\Helpers\Http;

class RecaptchaController extends Controller
{
    public function verify(Request $request)
    {
        $request->validate([
            'response' => 'required|string'
        ]);

        $resp = Http::post(config('auth-module.recaptcha.verification_url'), [
            'form_params' => [
                'secret' => config('auth-module.recaptcha.key'),
                'response' => $request->input('response'),
            ],
        ]);

        if ($resp['success']) {
            return Respond::success();
        }

        return Respond::error(
            _t('Something went wrong'),
            array_map(function ($code) {
                return $this->getErrorMessage($code);
            }, $resp['error-codes']),
        );
    }

    protected function getErrorMessage(string $code)
    {
        return [
            'missing-input-secret' => _t('The secret parameter is missing.'),
            'invalid-input-secret' => _t('The secret parameter is invalid or malformed.'),
            'missing-input-response' => _t('The response parameter is missing.'),
            'invalid-input-response' => _t('The response parameter is invalid or malformed.'),
            'bad-request' => _t('The request is invalid or malformed.'),
            'timeout-or-duplicate' => _t('The response is no longer valid: either is too old or has been used previously.'),
        ][$code] ?? $code;
    }
}
