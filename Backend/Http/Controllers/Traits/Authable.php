<?php

namespace Module\Auth\Backend\Http\Controllers\Traits;

use Larasar\Helpers\Respond;
use Module\Auth\Backend\Models\User;
use Illuminate\Http\JsonResponse;

trait Authable
{
  protected function getToken(User $user): string
  {
    return $user->createToken(request()->source ?? $user->email)->plainTextToken;
  }

  protected function tokenResponse(User $user): JsonResponse
  {
    return Respond::success([
      'token' => $this->getToken($user),
    ]);
  }
}
