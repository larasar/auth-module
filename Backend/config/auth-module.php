<?php

return [
  'recaptcha' => [
    'key' => env('RECAPTCHA_KEY'),
    'verification_url' => env('RECAPTCHA_VERIFICATION_URL', 'https://www.google.com/recaptcha/api/siteverify'),
  ],
];
